// Enforce prefixing classes to avoid collisions with 3rd Party code
// Could be extended to `.no-...`,`.is-...`,`.has-...` state classes
// however best to either:
// - use BEM modifiers, to avoid internal collisions
// - rely on a data attribute
const PERMITTED_CLASS_NAME_PREFIXES = ['lch', 'dialog', 'tippy', 'no'].join(
  '|',
);
const KEBAB_CASE_BEM =
  '[a-z0-9]+(-(?!-)[a-z0-9]+)*(--[a-z0-9]+(-(?!-)[a-z0-9]+)*)?(__[a-z0-9]+(-(?!-)[a-z0-9]+)*)?';

module.exports = {
  extends: ['stylelint-config-twbs-bootstrap', 'stylelint-config-prettier'],
  rules: {
    // Bump number of allowed universal selectors to enable `* + *`
    'selector-max-universal': 2,
    // Enforce prefixing and kebab-cased BEM naming for classes
    'selector-class-pattern': new RegExp(
      `^(${PERMITTED_CLASS_NAME_PREFIXES})-${KEBAB_CASE_BEM}$`,
    ),
    'custom-property-pattern': new RegExp(KEBAB_CASE_BEM),
    // We're not building a library so no need to use `!default` for the variables
    'scss/dollar-variable-default': null,
    // Only allow to extend placeholder selectors to prevent unwanted side effects
    'scss/at-extend-no-missing-placeholder': true,
    // Sass' will consider `min` or `max` to be its own `min`/`max` functions
    // rather than CSS ones. Using the capitalized version ensures Sass leaves
    // the calls alone and doesn't try to run its own functions.
    'function-name-case': [
      'lower',
      {
        ignoreFunctions: [/[mMin]/, /[mMax]/],
      },
    ],
  },
  overrides: [
    {
      files: ['*.svelte', '**/*.svelte'],
      extends: [
        ...require('stylelint-config-twbs-bootstrap').overrides[0].extends,
        'stylelint-config-html/svelte', // Comes second, otherwise SCSS rules are not recognized
        'stylelint-config-prettier',
      ],
      rules: require('stylelint-config-twbs-bootstrap').overrides[0].rules,
    },
  ],
};
