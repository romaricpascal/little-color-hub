import tailwindCSS from './tailwindcss.js';
import openColor from './open-color.js';
import componentsLCH from './components.ai/lch_natural.js';
import componentsRGB from './components.ai/rgb_natural.js';
import bootstrap from './bootstrap.js';
import reasonableColors from './reasonable-colors.js';

export default {
	bootstrap,
	tailwindCSS,
	openColor,
	componentsLCH,
	componentsRGB,
	reasonableColors,
};
