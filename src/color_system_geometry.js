import { lch, rgb } from 'culori';
import { mean } from 'd3-array';
import { BufferGeometry, BufferAttribute, Vector3 } from 'three';

import { pointFromColor } from './lib/color_point.js';

/**
 * A 3D geometry representing a color system as a wrapping
 * surface, with:
 * - each color set forming a "longitude" on the surface
 * - each color index forming a "latitude" on the surface
 *
 * Assumes sets have equal amounts of colors between them.
 */
export class ColorSystemGeometry extends BufferGeometry {
	constructor(colorSystem) {
		super();
		this.colorSystem = colorSystem;

		this.setFromPoints(this.vertices.map((vertex) => vertex.position));

		const vertexColors = [];
		for (const vertex of this.vertices) {
			vertexColors.push(...vertex.rgb);
		}
		this.setAttribute(
			'color',
			new BufferAttribute(new Float32Array(vertexColors), 4),
		);

		this.setIndex(
			geometryIndex({
				numberOfSets: this.numberOfSets,
				colorsPerSet: this.colorsPerSet,
				numberOfVertices: this.vertices.length,
			}),
		);
	}

	get data() {
		if (!this._data) {
			this._data = Object.entries(this.colorSystem)
				.filter(([name]) => !name.match(/gray/))
				.map(([name, colors]) => {
					if (!Array.isArray(colors)) {
						colors = Object.values(colors);
					}

					// Need LCH for ordering by hue to create the mesh
					// Not passing directly `lch` to not have the call polluted
					// by map's extra params (index and source array)
					colors = colors
						.map((c) => lch(c))
						// Ensure consistent ordering between color syste
						.sort(({ l: a }, { l: b }) => a - b);

					// Hues representing a circular value, we need a slightly
					// different way to compute the mean so that series with values
					// on both sides of 360/0deg average correctly.
					// This calls for first transforming the angles in gradient
					const averageHue = circularMean(
						colors.map((color) => (color.h * Math.PI) / 180),
					);

					return {
						name,
						colors,
						averageHue,
					};
				})
				.filter(({ averageHue }) => !!averageHue)
				.sort(({ averageHue: a }, { averageHue: b }) => {
					return a - b;
				});
		}

		return this._data;
	}

	get numberOfSets() {
		return this.data.length;
	}

	get colorsPerSet() {
		return this.data[0].colors.length;
	}

	get vertices() {
		if (!this._vertices) {
			const allColors = this.data.map(({ colors }) => colors).flat();

			this._vertices = allColors.map((color) => {
				const rgbColor = rgb(color);
				return {
					position: pointFromColor(color),
					// RGB color for 3D rendering to render the colors on the mesh
					rgb: [rgbColor.r, rgbColor.g, rgbColor.b, 1],
					// LCH color for computing intersection
					lch: lch(rgbColor),
				};
			});
		}

		return this._vertices;
	}

	get centroid() {
		if (!this._centroid) {
			this._centroid = new Vector3(
				mean(this.vertices.map((vertex) => vertex.position.x)),
				mean(this.vertices.map((vertex) => vertex.position.y)),
				mean(this.vertices.map((vertex) => vertex.position.z)),
			);
		}

		return this._centroid;
	}

	get faces() {
		if (!this._faces) {
			this._faces = this.computeFaces();
		}
		return this._faces;
	}

	computeFaces() {
		const result = [];
		for (var i = 0; i < this.index.count; i += 3) {
			const a = this.index.getX(i);
			const b = this.index.getX(i + 1);
			const c = this.index.getX(i + 2);

			result.push({ a, b, c });
		}

		return result;
	}
}

/**
 * Creates an index for a BufferedGeometry to create a wrapping surface
 * of colors, creating two triangles to bridge each two consecutive colors
 * of each two consecutive color sets.
 *
 * Triangles are built with their normal facing outwards.
 *
 * Their edges are set to facilitate intersection computations with:
 * - a-b bridging consecutive colors in the same set
 * - b-c bridging the same index in consecutive color sets
 * @param {Integer} options.numberOfSets
 * @param {Integer} options.colorsPerSet
 * @param {Integer} numberOfVertices
 * @returns Array.<Integer>
 */
function geometryIndex({ numberOfSets, colorsPerSet, numberOfVertices }) {
	const index = [];

	for (let set = 0; set < numberOfSets; set++) {
		for (let colorInSet = 0; colorInSet < colorsPerSet - 1; colorInSet++) {
			const bottomLeftIndex = set * colorsPerSet + colorInSet;
			const topLeftIndex = bottomLeftIndex + 1;
			const bottomRightIndex =
				(bottomLeftIndex + colorsPerSet) % numberOfVertices;
			const topRightIndex = bottomRightIndex + 1;

			index.push(topLeftIndex, bottomLeftIndex, bottomRightIndex);
			index.push(bottomRightIndex, topRightIndex, topLeftIndex);
		}
	}

	return index;
}

export function indicesOfEdgeBetweenSets(numberOfSets, colorsPerSet) {
	const result = [];

	for (let set = 0; set < numberOfSets; set++) {
		for (let colorInSet = 0; colorInSet < colorsPerSet; colorInSet++) {
			const start = set * colorsPerSet + colorInSet;
			const end = ((set + 1) % numberOfSets) * colorsPerSet + colorInSet;
			result.push([start, end]);
		}
	}

	return result;
}

export function indicesOfMeridians(startColorSet, endColorSet, colorsPerSet) {
	const meridians = [];

	for (let colorIndex = 0; colorIndex < colorsPerSet; colorIndex++) {
		meridians.push([
			startColorSet * colorsPerSet + colorIndex,
			endColorSet * colorsPerSet + colorIndex,
		]);
	}

	return meridians;
}

/**
 * https://en.wikipedia.org/wiki/Circular_mean
 * @param  {...any} angles - The series of angles (*in radians*) to average
 * @returns The circular mean of the angles
 */
function circularMean(angles) {
	// prettier-ignore
	return Math.atan2(
    mean(angles.map(Math.sin)), 
    mean(angles.map(Math.cos))
  );
}
