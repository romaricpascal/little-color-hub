import { Vector3, Line3, Plane } from 'three';
import { derived, writable } from 'svelte/store';
import { pointFromColor } from '../color_point.js';
import { closestPointToPoint } from '../three/geometry/closest_point_to_point.js';
import { contour } from '../../color_set.js';

export const geometry = writable();

export const selectedColor = writable({
	mode: 'lch',
	l: 80,
	c: 100,
	h: 107,
});

export const selectedColorPosition = derived(selectedColor, pointFromColor);

export const intersectionOverride = writable();
export const intersection = derived(
	[geometry, selectedColorPosition, intersectionOverride],
	([geometryValue, selectedColorPositionValue, intersectionOverrideValue]) => {
		if (intersectionOverrideValue) {
			return intersectionOverrideValue;
		}
		if (geometryValue) {
			return closestPointToPoint(geometryValue, selectedColorPositionValue, {
				ignoreFace: ignoreFaceBehindPoint,
				closestFacePointToPoint: closestTrianglePointToPointWithSameHue(
					selectedColorPositionValue,
				),
			});
		}
	},
);

// In case the shape is concave, `closestPointToPoint` will lead to hue shifts,
// with the closest point being on the "edge" of the concavity rather than closer
// to the bowl, like it would make sense
function closestTrianglePointToPointWithSameHue(point) {
	// Because the point remains constant during the iteration
	// we can avoir creating some of the components of the computation
	// again and again

	// A plane at constant hue for picking the
	const plane = new Plane().setFromCoplanarPoints(
		new Vector3(),
		new Vector3(0, 1, 0),
		point,
	);

	const intersectionWithBC = new Vector3();
	const intersectionWithAC = new Vector3();

	const intersectionSegment = new Line3(intersectionWithBC, intersectionWithAC);

	return function (triangle, ignored, target) {
		// First let's check that we're not on the triangle itself
		// In which case we can drop slicing computations
		// (plus those are a bit flakey in that scenario)
		if (isOnTriangle(triangle, point)) {
			// We may be slightly off the triangle
			// so we still need to get a point properly on the triangle
			// for contour computations to work properly
			return triangle.closestPointToPoint(point, target);
		}

		// Thanks to how the geometry is constructed,
		// we need to use the plane to intersect with:
		// - BC: The segment part of the mesh's "parallels"
		// - AC: The "diagonal" segment between "meridians"
		const BC = new Line3(triangle.b, triangle.c);
		const AC = new Line3(triangle.a, triangle.c);

		const planeIntersectsBC = plane.intersectLine(BC, intersectionWithBC);
		const planeIntersectsAC = plane.intersectLine(AC, intersectionWithAC);

		if (planeIntersectsBC && planeIntersectsAC) {
			return intersectionSegment.closestPointToPoint(point, true, target);
		}
	};
}

// Because the shape is irregular and not centred on the Y axis
// We need to make sure we only consider faces "in front" of the selected
// color. Otherwise, the closest face may be of the opposite hue,
// which is mathematically correct, but not what we're after
function ignoreFaceBehindPoint(triangle, point) {
	return triangle.isFrontFacing(point);
}

// `Triangle.containsPoint` is not enough to know if the point is
// on the triangle completely as it projects the point onto the triangle
// before checking, so we need to ensure we're actually on the triangle
// using a dot product
const trianglePlane = new Plane();
function isOnTriangle(triangle, point) {
	return (
		triangle.containsPoint(point) &&
		Math.abs(triangle.getPlane(trianglePlane).distanceToPoint(point)) < 0.01
	);
}

export const extrapolatedPoints = derived(
	[geometry, intersection],
	([geometryValue, intersectionValue]) => {
		if (intersectionValue) {
			return contour(geometryValue, intersectionValue);
		} else {
			return [];
		}
	},
);

export const colorToContrastAgainst = writable({
	mode: 'lch',
	l: 25,
	c: 50,
	h: 35,
});
