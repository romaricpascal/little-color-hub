import { WebGLRenderer, WebGLRenderTarget } from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { OutlineMapPass } from './outline_map_pass.js';
import { OutlineFromMapPass } from './outline_from_map_pass.js';
import { SMAAPass } from 'three/examples/jsm/postprocessing/SMAAPass.js';

export class PostProcessingRenderer {
	constructor({ scene, camera, canvas }) {
		this.renderer = new WebGLRenderer({
			canvas: canvas,
		});
		this.renderer.setClearColor(0x1e0123, 1);

		this.composer = new EffectComposer(
			this.renderer,
			this.createRenderTarget(),
		);

		const renderPass = new RenderPass(scene, camera);
		this.composer.addPass(renderPass);

		this.outlineMapPass = new OutlineMapPass(
			scene,
			camera,
			this.createRenderTarget(),
		);
		this.composer.addPass(this.outlineMapPass);

		// Don't pass the outlineMapPass's renderTarget as it may get changed
		// after changes of pixel ratio
		const outlineFromMapPass = new OutlineFromMapPass(this.outlineMapPass);
		this.composer.addPass(outlineFromMapPass);

		if (!this.renderer.capabilities.isWebGL2) {
			this.smaaPass = new SMAAPass();
			this.smaaPass.enabled = this.needsAntiAliasing;
			this.composer.addPass(this.smaaPass);
		}
	}

	render() {
		this.composer.render();
	}

	setSize(width, height) {
		this.renderer.setSize(width, height);
		this.composer.setSize(width, height);
		// The composer will forward the set size to the passes
		// so no need to do it ourselves (especially as it'll factor
		// the pixel ratio in the size it sets them)
	}

	setPixelRatio(pixelRatio) {
		this.renderer.setPixelRatio(pixelRatio);
		this.composer.setPixelRatio(pixelRatio);
		// The composer will take care of forwarding an effective size
		// to the passes via their `setSize` so no need to sort anything
		// ourselves for that.

		// We do need to update the need for an SMAAPass though
		if (this.smaaPass) {
			this.smaaPass.enabled = this.needsAntiAliasing;
		}

		// And check if we need new render targets
		if (this.needsNewRenderTarget) {
			this.composer.reset(this.createRenderTarget());
			this.outlineMapPass.renderTarget = this.createRenderTarget();
		}
	}

	get needsAntiAliasing() {
		return this.renderer.getPixelRatio() < 2;
	}

	get samples() {
		return this.needsAntiAliasing ? 2 : 0;
	}

	createRenderTarget({ ...options } = {}) {
		// Size doesn't really matter here as it'll get update
		// straight after instantiation
		return new WebGLRenderTarget(800, 600, {
			samples: this.samples,
			...options,
		});
	}

	get needsNewRenderTarget() {
		return this.composer.renderTarget1.samples !== this.samples;
	}
}
