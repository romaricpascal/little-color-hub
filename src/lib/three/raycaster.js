export function getFirstIntersection(raycaster, mesh) {
	const intersections = raycaster.intersectObject(mesh);
	if (intersections && intersections[0]) {
		return intersections[0];
	}
}
