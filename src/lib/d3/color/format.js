import { lch, hsl, formatHex, formatRgb, formatCss } from 'culori';
import { withNumberPrecision } from '../../string.js';

const FORMATTERS = {
	lch(color) {
		return withNumberPrecision(formatCss(lch(color)), 2);
	},
	hsl(color) {
		return withNumberPrecision(formatCss(hsl(color)), 2);
	},
	rgb(color) {
		return formatRgb(color);
	},
	hex(color) {
		return formatHex(color);
	},
};

export function formatColor(
	color,
	{ format = 'lch', ...formatterOptions } = {},
) {
	return FORMATTERS[format](color, formatterOptions);
}
