export function toDataUrl(
	content,
	{ format, mimeType = toMimeType(format), base64 = true },
) {
	return [
		'data:',
		mimeType,
		base64 ? ';base64' : '',
		',',
		base64 ? window.btoa(content) : content,
	].join('');
}

const MIME_TYPES = {
	css: 'text/css',
	scss: 'text/css',
	json: 'application/json', // There doesn't seem to be any official mime type for SCSS
};

function toMimeType(format) {
	return MIME_TYPES[format] || '';
}
