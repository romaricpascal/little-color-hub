export default function toScss(colors, { colorSetName = 'colors' } = {}) {
	return colors.map(toScssVariable(colorSetName)).join('\n');
}

function toScssVariable(colorSetName) {
	return function (color, index) {
		return `$${colorSetName}-${index}: ${color};`;
	};
}
