export default function toJson(colors, { colorSetName = 'colors' } = {}) {
	return JSON.stringify(
		{
			[colorSetName]: colors.map((c) => c.toString()),
		},
		null,
		2,
	);
}
