/**
 * Creates and dispatch an event of the given type from the given element.
 * Events bubble and are cancellable by default, but can be overriden
 * using the functions `options` argument.
 *
 * @param {HTMLElement} element
 * @param {String} eventType
 * @param {CustomEventInit} options
 * @returns The event that has been dispatched, allowing to check for its `defaultPrevented` for ex.
 */
export function dispatch(element, eventType, options = {}) {
	const event = new CustomEvent(eventType, {
		bubbles: true,
		cancelable: true,
		...options,
	});
	element.dispatchEvent(event);
	return event;
}
