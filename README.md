Little Color Hub
===

Little Color Hub is a **prototype project** that lets you fit a custom colors into an existing system of colors:
- [Tailwind](https://tailwindcss.com/docs/customizing-colors#color-palette-reference), 
- [Bootstrap](https://getbootstrap.com/docs/5.0/customize/color/)
- [Reasonable colors](https://reasonable.work/colors/)
- [Components.ai](https://components.ai/theme/LCH-natural)
- [Open color](https://yeun.github.io/open-color/) 

Feed it a color of your choice and it'll provide you with a scale of color within the system you chose with as close as possible a hue to it. Your brand color can now be declined with the same variations as the other colors in the system!

For each color it generates, you'll also get which contrast thresholds they meet (3, 4.5, 7) against white, 
black or a color of your choice. This'll let you easily pick the right one for:
- [text of minimum contrast](https://www.w3.org/WAI/WCAG21/quickref/#contrast-minimum), 
- [icons, control borders and other graphics](https://www.w3.org/WAI/WCAG21/quickref/#non-text-contrast), 
- [focus indicator of minimum contrast](https://www.w3.org/TR/WCAG22/#focus-appearance-minimum)
- [text of enhanced contrast](https://www.w3.org/WAI/WCAG21/quickref/#contrast-enhanced)
- [focus indicator of enhanced contrast](https://www.w3.org/TR/WCAG22/#focus-appearance-enhanced)


How it works
---

After transforming the colors into the [LCH color space](https://lea.verou.me/2020/04/lch-colors-in-css-what-why-and-how/),
Little Color Hub uses each color as the coordinates to create a 3D Mesh of the color system (click Explore 3D to see how it looks).

It then projects your chosen color on that 3D mesh, to find the closest color with the same hue. Once it's got that reference color in the system, it extrapolates a corresponding set by following the shape of the mesh and stopping where the colors are.


Thanks and acknowledgements
---

None of this would be possible without:

- the [ThreeJS library](https://threejs.org/) to take care of all the 3D maths (and its rendering of course)
- Bruno Simon's [ThreeJS journey](https://threejs-journey.com) course that was a massive help understanding shaders for the rendering of the outlines in the 3D view
- The [culori.js](https://culorijs.org/) library for handling the color maths
- [Svelte](https://svelte.dev/) for rendering the UI
- The [Sono](https://github.com/sursly/sono) font, one of the many gorgeous fonts by [Ty Finck](https://www.tyfromtheinternet.com/)
- [Inkscape](https://inkscape.org/) that let me draw the project's lovely (I hope) logo.


Project status
---

- Deployment: **live at <https://little-color-hub.com>**.
- Development: **paused for the moment** following the last tidy ups before release. I may come back to it at some point... or maybe not... I'll see how I feel.
- Licence: [MIT Licence](https://mit-license.org/)

© Romaric Pascal, 2022
