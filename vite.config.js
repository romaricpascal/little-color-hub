import { svelte } from '@sveltejs/vite-plugin-svelte';
import svgLoader from 'vite-svg-loader';
import svelteConfig from './svelte.config.js';
import rollupOptions from './rollup.config.js';

export default {
	resolve: {
		alias: [
			{
				find: /^three$/,
				replacement: 'three/src/Three.js',
			},
		],
	},
	plugins: [
		svelte(svelteConfig),
		svgLoader({
			defaultImport: 'raw',
		}),
	],
	build: {
		rollupOptions,
	},
};
